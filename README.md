# ToddLininger_ClassManagerGraphQl

GraphQL API module for ToddLininger [Class Manager](https://classrosters.com) extension.

## Installation
**1) Copy-to-paste method**
- Download this module and upload it to the `app/code/ToddLininger/ClassManagerGraphQl` directory

**2) Installation using composer (from packagist)**
- Execute the following command: `composer require toddlininger/classmanager-graphql`

## How to use
Coming soon
