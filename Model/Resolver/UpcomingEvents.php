<?php

declare(strict_types=1);

namespace ToddLininger\ClassManagerGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class UpcomingEvents implements ResolverInterface
{
    /**
     * @var \ToddLininger\ClassManagerGraphQl\Model\Session\PrepareSessionsData
     */
    private $prepareSessionsData;

    /**
     * @var \ToddLininger\ClassManager\Api\SessionRepositoryInterfaceFactory
     */
    private $repositoryInterfaceFactory;

    public function __construct(
        \ToddLininger\ClassManagerGraphQl\Model\Session\PrepareSessionsData $prepareSessionsData,
        \ToddLininger\ClassManager\Api\SessionRepositoryInterfaceFactory $repositoryInterfaceFactory
    ) {
        $this->prepareSessionsData = $prepareSessionsData;
        $this->repositoryInterfaceFactory = $repositoryInterfaceFactory;
    }

    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ): array {
        $count = $args['count'] ?? 8;
        $repository = $this->repositoryInterfaceFactory->create();
        $items = $repository->getUpcomingList($count)->getItems();

        return $this->prepareSessionsData->execute($items);
    }
}
