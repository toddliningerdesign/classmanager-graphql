<?php

declare(strict_types=1);

namespace ToddLininger\ClassManagerGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlAuthorizationException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class UpcomingSessions implements ResolverInterface
{
    /**
     * @var \ToddLininger\ClassManager\Model\CustomerRegistrationRepository
     */
    private $customerRegistrationRepository;

    /**
     * @var \ToddLininger\ClassManagerGraphQl\Model\Session\PrepareSessionsData
     */
    private $prepareSessionsData;

    public function __construct(
        \ToddLininger\ClassManager\Model\CustomerRegistrationRepository $customerRegistrationRepository,
        \ToddLininger\ClassManagerGraphQl\Model\Session\PrepareSessionsData $prepareSessionsData
    ) {
        $this->customerRegistrationRepository = $customerRegistrationRepository;
        $this->prepareSessionsData = $prepareSessionsData;
    }

    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ): array {
        $currentUserId = (int)$context->getUserId();

        if (false === $context->getExtensionAttributes()->getIsCustomer()) {
            throw new GraphQlAuthorizationException(__('The request is allowed for logged in customer'));
        }

        if ($currentUserId) {
            $sessions = $this->customerRegistrationRepository->getUpcomingSessions($currentUserId)->getItems();
            $data = $this->prepareSessionsData->execute($sessions);
        } else {
            throw new GraphQlNoSuchEntityException(
                __('Customer is not authorized.')
            );
        }

        return $data;
    }
}
