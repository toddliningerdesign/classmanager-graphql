<?php

declare(strict_types=1);

namespace ToddLininger\ClassManagerGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlAuthorizationException;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use ToddLininger\ClassManager\Api\Data\RegistrationInterface;

class DeleteFromWaitList implements ResolverInterface
{
    /**
     * @var \ToddLininger\ClassManager\Api\RegistrationRepositoryInterfaceFactory
     */
    private $registrationRepositoryInterfaceFactory;

    /**
     * @var \ToddLininger\ClassManager\Model\ResourceModel\Registration\CollectionFactory
     */
    private $collectionFactory;

    public function __construct(
        \ToddLininger\ClassManager\Api\RegistrationRepositoryInterfaceFactory $registrationRepositoryInterfaceFactory,
        \ToddLininger\ClassManager\Model\ResourceModel\Registration\CollectionFactory $collectionFactory
    ) {
        $this->registrationRepositoryInterfaceFactory = $registrationRepositoryInterfaceFactory;
        $this->collectionFactory = $collectionFactory;
    }

    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ): \Magento\Framework\Phrase {
        $currentUserId = (int)$context->getUserId();

        if (empty($args['input']['session_id'])) {
            throw new GraphQlInputException(__('Required parameter "session_id" is missing'));
        }

        if (false === $context->getExtensionAttributes()->getIsCustomer()) {
            throw new GraphQlAuthorizationException(__('The request is allowed for logged in customer'));
        } else {
            try {
                /** @var \ToddLininger\ClassManager\Model\ResourceModel\Registration\Collection $_collection */
                $collection = $this->collectionFactory->create();
                $collection
                    ->addFilter(RegistrationInterface::FIELD_CUSTOMER_ID, $currentUserId)
                    ->addFilter(RegistrationInterface::FIELD_SESSION_ID, (int)$args['input']['session_id'])
                    ->addFilter(RegistrationInterface::FIELD_STATUS, RegistrationInterface::STATUS_WAIT_LIST)
                    ->load();

                if (!$collection->count()) {
                    return __("Your the wait list spot isn't found");
                } else {
                    /** @var \ToddLininger\ClassManager\Model\Registration $registration */
                    $registration = $collection->getFirstItem();
                    /** @var \ToddLininger\ClassManager\Model\RegistrationRepository $registrationRepository */
                    $registrationRepository = $this->registrationRepositoryInterfaceFactory->create();
                    $registrationRepository->delete($registration);
                }
            } catch (\Exception $e) {
                throw new GraphQlInputException(__($e->getMessage()));
            }
        }

        return __("You deleted the wait list spot for this class.");
    }
}
